import Vue from 'vue'
import Router from 'vue-router'
import Home from './Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/1',
      component: () => import(`./exercises/Exercise1.vue`)
    },
    {
      path: '/2',
      component: () => import(`./exercises/Exercise2.vue`)
    },
    {
      path: '/3',
      component: () => import(`./exercises/Exercise3.vue`)
    },
    {
      path: '/4',
      component: () => import(`./exercises/Exercise4.vue`)
    },
    {
      path: '/5',
      component: () => import(`./exercises/Exercise5.vue`)
    },
    {
      path: '/6',
      component: () => import(`./exercises/Exercise6.vue`)
    },
    {
      path: '/7',
      component: () => import(`./exercises/Exercise7.vue`)
    }
  ]
})
